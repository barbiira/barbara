//import the Chart constructor from the chart.js folder in node_modules
import Chart from "chart.js/auto";

//declare the context of the chart, passing it the id of the canvas I create in HTML
const ctx = document.getElementById("mychart").getContext("2d");

//declare the connection to receive data with the socket
const sck = new WebSocket("ws://localhost:9000");

//create two empty arrays that will be populated with the socket data
const data = [];
const labels = [];

//create the graphic
const chart = new Chart(ctx, {
    type: 'line',
    data: {
        labels,
        datasets: [{
            label: 'Test',
            data,
            fill: false,
            borderColor: "rgb(75, 192, 192)",
            tension: 0.1
        }],
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    }
});

//listen to the data provided by the backend, and then insert them in my empty array.
sck.addEventListener("message", e => {
    const axes = JSON.parse(e.data);
        data.push(+axes.value);
        labels.push(axes.time); 
});

//tell the chart how often it should update with a time interval
setInterval(() => { 
    data,
    labels,
    chart.update();
}, 500);